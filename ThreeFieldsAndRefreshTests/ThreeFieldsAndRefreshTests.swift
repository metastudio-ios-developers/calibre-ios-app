//
//  ThreeFieldsAndRefreshTests.swift
//  ThreeFieldsAndRefreshTests
//
//  Created by Rafael Kayumov on 22/08/14.
//  Copyright (c) 2014 Rafael Kayumov. All rights reserved.
//

import UIKit
import XCTest

class ThreeFieldsAndRefreshTests: KIFTestCase {
    
    var tester : KIFUITestActor {
        get {
            return KIFUITestActor(inFile: __FILE__, atLine: __LINE__, delegate: self)
        }
    }
    
    //first input server IP and user name
    //used in bith cases
    override func beforeAll() {
        self.inputServerIpAndUsername()
    }
    
    //after each case have to close alert view and clear password field
    override func afterEach() {
        tester.tapViewWithAccessibilityLabel("Ok");
        tester.clearTextFromViewWithAccessibilityLabel("Password Field")
    }
    
    func testSuccessfulCase() {
        tester.tapViewWithAccessibilityLabel("Password Field")
        tester.enterText("733cal", intoViewWithAccessibilityLabel: "Password Field")
        tester.tapViewWithAccessibilityLabel("Refresh Button")
        tester.waitForViewWithAccessibilityLabel("Success")
    }
    
    func testFailureCase() {
        tester.tapViewWithAccessibilityLabel("Password Field")
        tester.enterText("Wrong_Pass", intoViewWithAccessibilityLabel: "Password Field")
        tester.tapViewWithAccessibilityLabel("Refresh Button")
        tester.waitForViewWithAccessibilityLabel("Error")
    }
    
    func inputServerIpAndUsername() {
        tester.tapViewWithAccessibilityLabel("ServerIP Field")
        tester.enterText("203.219.7.174", intoViewWithAccessibilityLabel: "ServerIP Field")
        tester.tapViewWithAccessibilityLabel("Username Field")
        tester.enterText("cpritchett", intoViewWithAccessibilityLabel: "Username Field")
    }
}

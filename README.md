# Test task #

Simple single-view app written in Swift.

View contains 3 fields for settings:
* Server IP
* Username
* Password

Used KIF library for interface testing. KIF imported via CocoaPods. Use ``` pod update``` to load pods.

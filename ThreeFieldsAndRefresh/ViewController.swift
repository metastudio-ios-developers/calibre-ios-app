//
//  ViewController.swift
//  ThreeFieldsAndRefresh
//
//  Created by Rafael Kayumov on 22/08/14.
//  Copyright (c) 2014 Rafael Kayumov. All rights reserved.
//

import UIKit

enum MessageType {
    case Success
    case Error
    func simpleDescription() -> String {
        switch self {
        case .Success:
            return "Success"
        case .Error:
            return "Error"
        }
    }
}

class ViewController: UIViewController {
    
    @IBOutlet var serverIPField:UITextField?
    @IBOutlet var usernameField:UITextField?
    @IBOutlet var passwordField:UITextField?
                            
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func refresh() {
        var serverIP = self.serverIPField?.text
        var username = self.usernameField?.text
        var password = self.passwordField?.text
        RestOperator.sharedInstance.callServer(serverIP!, username: username!, password: password!, success:success, failure:failure)
    }
    
    func success(successObject: String) {
        //trimmed json string
        //too large string can't be displayed in alert view
        var substring = successObject.substringToIndex(advance(successObject.startIndex, 1000))
        self.displayMessage(MessageType.Success, message: substring)
    }
    
    func failure(error: NSError) {
        self.displayMessage(MessageType.Error, message: error.localizedDescription)
    }
    
    func displayMessage(messageType:MessageType, message:String) {
        dispatch_sync(dispatch_get_main_queue(),{
            var alert = UIAlertView(title: messageType.simpleDescription(), message: message, delegate: nil, cancelButtonTitle:"Ok")
            alert.show()
        })
    }
    
    @IBAction func onRefresh(sender:UIButton) {
        self.refresh()
    }
}


//
//  RestOperator.swift
//  ThreeFieldsAndRefresh
//
//  Created by Rafael Kayumov on 22/08/14.
//  Copyright (c) 2014 Rafael Kayumov. All rights reserved.
//

import UIKit

let restCallString = "7080/crmsREST/serverconfig"

class RestOperator: NSObject, NSURLSessionDelegate {
    
    var username:String?
    var password:String?
    
    var currentSession:NSURLSession {
        get {
            return NSURLSession(configuration: NSURLSessionConfiguration.defaultSessionConfiguration(), delegate: self, delegateQueue: nil)
        }
    }
    
    class var sharedInstance : RestOperator {
    struct Static {
        static var onceToken : dispatch_once_t = 0
        static var instance : RestOperator? = nil
        }
        dispatch_once(&Static.onceToken) {
            Static.instance = RestOperator()
        }
        return Static.instance!
    }
    
    func callServer(serverIP:String, username: String, password: String, success:String -> Void, failure:NSError -> Void) {
        self.username = username
        self.password = password
        
        var restURL = NSURL(string: String(format: "http://%@:%@", serverIP, restCallString))
        var restRequest = NSMutableURLRequest(URL: restURL)
        restRequest.HTTPMethod = "GET"
        
        var err: NSError?
        restRequest.addValue("application/json", forHTTPHeaderField: "Content-Type")
        restRequest.addValue("application/json", forHTTPHeaderField: "Accept")
        
        self.currentSession.finishTasksAndInvalidate()
        
        var task = self.currentSession.dataTaskWithRequest(restRequest, completionHandler: {data, response, error -> Void in
            var strData = NSString(data: data, encoding: NSUTF8StringEncoding)
            var err: NSError?
            
            if err? == nil && data? != nil && response? != nil {
                var jsonResult: NSDictionary! = NSJSONSerialization.JSONObjectWithData(data, options:NSJSONReadingOptions.MutableContainers, error: nil) as? NSDictionary
                success(String(format:"%@", jsonResult))
            } else {
                failure(error)
            }
        })
        task.resume()
    }
    
    func URLSession(session: NSURLSession!, task: NSURLSessionTask!, didReceiveChallenge challenge: NSURLAuthenticationChallenge!, completionHandler: ((NSURLSessionAuthChallengeDisposition, NSURLCredential!) -> Void)!){
        if challenge.previousFailureCount > 0 {
            println("Alert Please check the credential")
            completionHandler(NSURLSessionAuthChallengeDisposition.CancelAuthenticationChallenge, nil)
        } else {
            var credential = NSURLCredential(user:self.username, password:self.password, persistence: .ForSession)
            completionHandler(NSURLSessionAuthChallengeDisposition.UseCredential,credential)
        }
    }
}
 